<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControllerFetchdata;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/fetch',[ControllerFetchdata::class,'index']);
Route::post('/submit',[ControllerFetchdata::class,'submit']);
